# Makefile for libobjccurses
# Compiles the code for the library
# Created on 7/30/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the Lesser GNU General Public License, version 3

# define the compiler
CC=gcc

# define the compiler flags for the library
CFLAGS=`gnustep-config --objc-flags` -x objective-c -fPIC -c -Wall
CFLAGS += -Wno-unused-but-set-variable

# define the compiler flags for the test program
test: CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall -g

# define linker flags for the library
LDFLAGS=-shared -lncurses -lgnustep-base -lobjc 

# define linker flags for the test program
TFLAGS=-lgnustep-base -lobjc -L./lib -Wl,-rpath,./lib -lobjccurses

# retrieve source code for the library
LCTX=$(shell ls src/lib/context/*.m)
LIO=$(shell ls src/lib/io/*.m)
LWIN=$(shell ls src/lib/window/*.m)
LATTR=$(shell ls src/lib/attr/*.m)
LCOL=$(shell ls src/lib/color/*.m)
LEXCE=$(shell ls src/lib/except/*.m)
LMOUSE=$(shell ls src/lib/mouse/*.m)

# list the source code for the library
LSOURCES=$(LCTX) $(LIO) $(LWIN) $(LATTR) $(LCOL) $(LEXCE) $(LMOUSE)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.m=.o)

# retrieve source code for the test program
TMAIN=$(shell ls src/test/*.m)

# list the source code for the test program
TSOURCES=$(TMAIN)

# compile the source code for the test program
TOBJECTS=$(TSOURCES:.m=.o)

# define the name of the library
LIB=libobjccurses.so

# define the name of the test executable
TEST=demo

# rule for building both the library and the test program
all: library test

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test program
test: $(TSOURCES) $(TEST)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CC) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/
	find . -type f -name '*.d' -delete

# sub-rule for compiling the test program
$(TEST): $(TOBJECTS)
	$(CC) $(TOBJECTS) -o $@ $(TFLAGS)
	mkdir bin
	mkdir tobj
	mv -f $(TOBJECTS) tobj/
	mv -f $@ bin/
	find . -type f -name '*.d' -delete

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libobjccurses.so" ]; then \
		rm /usr/lib/libobjccurses.so; \
	fi 
	if [ -d "/usr/include/objccurses" ]; then \
		rm -rf /usr/include/objccurses; \
	fi 
	
	mkdir /usr/include/objccurses
	mkdir /usr/include/objccurses/context
	mkdir /usr/include/objccurses/io 
	mkdir /usr/include/objccurses/window 
	mkdir /usr/include/objccurses/attr 
	mkdir /usr/include/objccurses/color 
	mkdir /usr/include/objccurses/except
	mkdir /usr/include/objccurses/mouse 
	cp src/lib/objccurses.h /usr/include/objccurses/
	cp $(shell ls src/lib/context/*.h) /usr/include/objccurses/context/
	cp $(shell ls src/lib/io/*.h) /usr/include/objccurses/io/
	cp $(shell ls src/lib/window/*.h) /usr/include/objccurses/window/
	cp $(shell ls src/lib/attr/*.h) /usr/include/objccurses/attr/
	cp $(shell ls src/lib/color/*.h) /usr/include/objccurses/color/
	cp $(shell ls src/lib/except/*.h) /usr/include/objccurses/except/
	cp $(shell ls src/lib/mouse/*.h) /usr/include/objccurses/mouse/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	if [ -d "tobj" ]; then \
		rm -rf tobj; \
	fi

# end of Makefile
