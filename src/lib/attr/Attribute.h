/*
 * Attribute.h
 * Declares a protocol for output attributes
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>
#import "AttributeType.h"
#import "../color/ColorPair.h"

@protocol Attribute <NSObject>

//required methods and properties
@required

//properties
@property (readonly) AttributeType type;
@property (readonly) bool enabled;

//methods

//enables an Attribute
- (void) enable;

//disables an Attribute
- (void) disable;

//combines an Attribute with another
- (void) combineWith: (id<Attribute>) other;

//optional methods
@optional

//returns the colors of an attribute
- (ColorPair*) colors;

@end
