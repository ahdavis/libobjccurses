/*
 * AttributeType.h
 * Enumerates types of ncurses attributes
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//include
#include <ncurses.h>

//enum definition
typedef enum {
	ATTR_NORMAL = A_NORMAL, //normal display
	ATTR_STANDOUT = A_STANDOUT, //highlighted display
	ATTR_UNDERLINE = A_UNDERLINE, //underlined text
	ATTR_REVERSE = A_REVERSE, //reverse video
	ATTR_BLINK = A_BLINK, //blinking output
	ATTR_DIM = A_DIM, //half bright
	ATTR_BOLD = A_BOLD, //extra bright or bold
	ATTR_PROTECT = A_PROTECT, //protected mode
	ATTR_INVIS = A_INVIS, //enable invisible or blank mode
	ATTR_ACS = A_ALTCHARSET, //enable alternate character set mode
	ATTR_COLOR //color attribute
} AttributeType;

//end of enum
