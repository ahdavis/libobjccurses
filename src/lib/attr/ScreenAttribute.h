/*
 * ScreenAttribute.h
 * Declares a class that represents an output screen attribute
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "AttributeType.h"
#import "Attribute.h"

@interface ScreenAttribute : NSObject 
		<Attribute> {
	//fields
	AttributeType _type; //what attribute is this?
	bool _enabled; //is the attribute enabled?
}

//property declarations
@property (readonly) AttributeType type;
@property (readonly) bool enabled;

//method declarations

//initialzes a ScreenAttribute instance with the NORMAL type
- (id) init;

//initializes a ScreenAttribute instance with a specified type
- (id) initWithType: (AttributeType) newType;

//initializes a ScreenAttribute instance 
//with a specified type and enabled state
- (id) initWithType: (AttributeType) newType enabled: (bool) newEnabled;

//copies a ScreenAttribute instance
- (id) copy;

//deallocates a ScreenAttribute instance
- (void) dealloc;

//enables a ScreenAttribute instance
- (void) enable;

//disables a ScreenAttribute instance
- (void) disable;

//combines an Attribute instance with another Attribute instance
- (void) combineWith: (id<Attribute>) other;

@end
