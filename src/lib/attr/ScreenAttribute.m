/*
 * ScreenAttribute.m
 * Implements a class that represents an output screen attribute
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "ScreenAttribute.h"

//import the ncurses header
#import <ncurses.h>


@implementation ScreenAttribute

//property synthesis
@synthesize type = _type;
@synthesize enabled = _enabled;

//first init method - initializes the ScreenAttribute with the NORMAL type
- (id) init {
	//call the second init method
	return [self initWithType: ATTR_NORMAL];
}

//second init method - initializes the ScreenAttribute 
//with a specified type
- (id) initWithType: (AttributeType) newType {
	//call the third init method
	return [self initWithType: newType enabled: NO];
}

//third init method - initializes the ScreenAttribute with a specified type
//and enabled state
- (id) initWithType: (AttributeType) newType enabled: (bool) newEnabled {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = newType;
		_enabled = newEnabled;

		//and determine whether to enable the attribute
		if(_enabled) { //if the attribute should be enabled
			attron(_type); //then enable it
		} 
	}

	//and return the instance
	return self;
}

//copy method - copies a ScreenAttribute instance
- (id) copy {
	//return a copy of the Attribute
	return [[ScreenAttribute alloc] 
		initWithType: _type enabled: _enabled];
}

//dealloc method - deallocates a ScreenAttribute instance
- (void) dealloc {
	[self disable]; //disable the Attribute
	[super dealloc]; //and call the superclass dealloc method
}

//enable method - enables the ScreenAttribute
- (void) enable {
	//determine whether the ScreenAttribute was already enabled
	if(!_enabled) { //if the ScreenAttribute was not enabled
		//then enable it
		_enabled = YES;
		attron(_type);
	}
}

//disable method - disables the ScreenAttribute
- (void) disable {
	//determine whether the ScreenAttribute was already disabled
	if(_enabled) { //if the ScreenAttribute was enabled
		//then disable it
		_enabled = NO;
		attroff(_type);
	}
}

//combineWith method - combines the Attribute with another
- (void) combineWith: (id<Attribute>) other {
	//combine the Attributes with a bitwise OR
	if([other type] == ATTR_COLOR) {
		_type |= COLOR_PAIR([[other colors] ID]);
	} else {
		_type |= [other type];
	}
}

@end
