/*
 * ScreenColorAttribute.h
 * Declares a class that represents a screen color attribute
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "Attribute.h"
#import "AttributeType.h"
#import "../color/ColorPair.h"

@interface ScreenColorAttribute : NSObject 
		<Attribute> {
	//fields
	AttributeType _type; //the type of the attribute
	bool _enabled; //is the attribute enabled?
	ColorPair* _colors; //what colors make up the attribute?
}

//property declarations
@property (readonly) AttributeType type;
@property (readonly) bool enabled;
@property (readonly) ColorPair* colors;

//method declarations

//initializes a ScreenColorAttribute instance
- (id) initWithColors: (ColorPair*) newColors;

//initializes a ScreenColorAttribute with the option to enable it
- (id) initWithColors: (ColorPair*) newColors enabled: (bool) newEnabled;

//copies a ScreenColorAttribute instance
- (id) copy;

//deallocates a ScreenColorAttribute instance
- (void) dealloc;

//enables the attribute
- (void) enable;

//disables the attribute
- (void) disable;

//combines the attribute with another
- (void) combineWith: (id<Attribute>) other;

@end
