/*
 * ScreenColorAttribute.m
 * Implements a class that represents a screen color attribute
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "ScreenColorAttribute.h"

//import the ncurses header
#import <ncurses.h>

//import the ColorModificationException header
#import "../except/ColorModificationException.h"

@implementation ScreenColorAttribute

//property synthesis
@synthesize type = _type;
@synthesize enabled = _enabled;
@synthesize colors = _colors;

//first init method - initializes a ScreenColorAttribute instance
- (id) initWithColors: (ColorPair*) newColors {
	//call the other init method
	return [self initWithColors: newColors enabled: NO];
}

//second init method - initializes a ScreenColorAttribute instance
//with the option to enable it
- (id) initWithColors: (ColorPair*) newColors enabled: (bool) newEnabled {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = ATTR_COLOR;
		_enabled = newEnabled;
		_colors = newColors;
		[_colors retain];

		//and determine whether to enable the attribute
		if(_enabled) { //if the attribute should be enabled
			//then enable it
			attron(COLOR_PAIR([_colors ID]));
		}
	}

	//and return the instance
	return self;
}

//copy method - copies the attribute
- (id) copy {
	//return a copy of the instance
	return [[ScreenColorAttribute alloc] initWithColors: [_colors copy]
			enabled: _enabled];
}

//dealloc method - deallocates the attribute
- (void) dealloc {
	[self disable]; //disable the attribute
	[_colors release]; //release the colors
	[super dealloc]; //and call the superclass dealloc method
}

//enable method - enables the attribute
- (void) enable {
	//determine whether the attribute is already enabled
	if(!_enabled) { //if the attribute is disabled
		//then enable it
		_enabled = YES;
		attron(COLOR_PAIR([_colors ID]));
	}
}

//disable method - disables the attribute
- (void) disable {
	//determine whether the attribute is already disabled
	if(_enabled) { //if the attribute is enabled
		//then disable it
		_enabled = NO;
		attroff(COLOR_PAIR([_colors ID]));
	}
}

//combineWith method - combines the attribute with another
- (void) combineWith: (id<Attribute>) other {
	//throw an exception
	ColorModificationException* exception =
		[ColorModificationException exception];

	@throw exception;
}

@end
