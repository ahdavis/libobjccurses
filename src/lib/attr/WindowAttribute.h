/*
 * WindowAttribute.h
 * Declares a class that represents an output attribute for a window
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "Attribute.h"
#import "AttributeType.h"
#import "../window/Window.h"

@interface WindowAttribute : NSObject
		<Attribute> {
	//fields
	AttributeType _type; //what type of attribute is this?
	bool _enabled; //is the attribute enabled?
	Window* _window; //the window that the attribute applies to
}

//property declarations
@property (readonly) AttributeType type;
@property (readonly) bool enabled;
@property (nonatomic, readonly) Window* window;

//method declarations

//initializes the WindowAttribute with the NORMAL attribute type
- (id) initWithWindow: (Window*) newWindow;

//initializes the WindowAttribute with 
//a programmer-specified attribute type
- (id) initWithWindow: (Window*) newWindow 
	andType: (AttributeType) newType;

//initializes the WindowAttribute with a programmer-specified
//attribute type along with the option to enable the attribute
- (id) initWithWindow: (Window*) newWindow
	andType: (AttributeType) newType enabled: (bool) newEnabled;

//copies the WindowAttribute
- (id) copy;

//deallocates the WindowAttribute
- (void) dealloc;

//enables the WindowAttribute
- (void) enable;

//disables the WindowAttribute
- (void) disable;

//combines the WindowAttribute with another Attribute
- (void) combineWith: (id<Attribute>) other;

@end
