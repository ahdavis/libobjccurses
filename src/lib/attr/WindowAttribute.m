/*
 * WindowAttribute.m
 * Implements a class that represents an output attribute for a window
 * Created on 8/7/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "WindowAttribute.h"

//import the ncurses header
#import <ncurses.h>

@implementation WindowAttribute

//property synthesis
@synthesize type = _type;
@synthesize enabled = _enabled;
@synthesize window = _window;

//first init method - initializes a WindowAttribute with the NORMAL type
- (id) initWithWindow: (Window*) newWindow {
	//call the second init method
	return [self initWithWindow: newWindow andType: ATTR_NORMAL];
}

//second init method - initializes a WindowAttribute with a
//programmer-specified attribute type
- (id) initWithWindow: (Window*) newWindow 
	andType: (AttributeType) newType {
	//call the third init method
	return [self initWithWindow: newWindow andType: newType
		enabled: NO];
}

//third init method - initializes a WindowAttribute with a
//programmer-specified attribute type and the option to enable it
- (id) initWithWindow: (Window*) newWindow
	andType: (AttributeType) newType enabled: (bool) newEnabled {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = newType;
		_enabled = newEnabled;
		_window = newWindow;
		[_window retain];

		//and determine whether to enable the attribute
		if(_enabled) { //if the attribute should be enabled
			wattron([_window data], _type); //then enable it
		}
	}

	//and return the instance
	return self;
}

//copy method - copies the WindowAttribute
- (id) copy {
	//return a copy of the WindowAttribute
	return [[WindowAttribute alloc] initWithWindow: _window
		andType: _type enabled: _enabled];
}

//dealloc method - deallocates the WindowAttribute
- (void) dealloc {
	[self disable]; //disable the attribute
	[_window release]; //release the window field
	[super dealloc]; //and call the superclass dealloc method
}

//enable method - enables the WindowAttribute
- (void) enable {
	//determine whether to enable the attribute
	if(!_enabled) { //if the attribute is disabled
		_enabled = YES; //then set the enabled flag
		wattron([_window data], _type); //and enable the attribute
	}
}

//disable method - disables the WindowAttribute
- (void) disable {
	//determine whether to disable the attribute
	if(_enabled) { //if the attribute is enabled
		_enabled = NO; //then clear the enabled flag
		wattroff([_window data], _type); //disable the attribute
	}
}

//combineWith method - combines the WindowAttribute with another attribute
- (void) combineWith: (id<Attribute>) other {
	//combine the types with a bitwise OR
	if([other type] == ATTR_COLOR) {
		_type |= COLOR_PAIR([[other colors] ID]);
	} else {
		_type |= [other type];
	}
}

@end
