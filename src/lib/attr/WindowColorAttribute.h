/*
 * WindowColorAttribute.h
 * Declares a class that represents a window color attribute
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "Attribute.h"
#import "AttributeType.h"
#import "../window/Window.h"
#import "../color/ColorPair.h"

@interface WindowColorAttribute : NSObject 
		<Attribute> {
	//fields
	AttributeType _type; //the type of the Attribute
	bool _enabled; //is the Attribute enabled?
	Window* _window; //the window that the Attribute applies to
	ColorPair* _colors; //the colors that make up the Attribute
}

//property declarations
@property (readonly) AttributeType type;
@property (readonly) bool enabled;
@property (nonatomic, readonly) Window* window; 
@property (nonatomic, readonly) ColorPair* colors;

//method declarations

//initializes the Attribute
- (id) initWithColors: (ColorPair*) newColors 
	andWindow: (Window*) newWindow;

//initializes the Attribute with the option to enable it
- (id) initWithColors: (ColorPair*) newColors
	andWindow: (Window*) newWindow enabled: (bool) newEnabled;

//copies the Attribute
- (id) copy;

//deallocates the Attribute
- (void) dealloc;

//enables the Attribute
- (void) enable;

//disables the Attribute
- (void) disable;

//combines the Attribute with another
- (void) combineWith: (id<Attribute>) other;

@end
