/*
 * WindowColorAttribute.m
 * Implements a class that represents a window color attribute
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "WindowColorAttribute.h"

//import the ncurses header
#import <ncurses.h>

//import the ColorModificationException header
#import "../except/ColorModificationException.h"

@implementation WindowColorAttribute

//property synthesis
@synthesize type = _type;
@synthesize enabled = _enabled;
@synthesize window = _window;
@synthesize colors = _colors;

//first init method - initializes the Attribute
- (id) initWithColors: (ColorPair*) newColors 
	andWindow: (Window*) newWindow {
	//call the other init method
	return [self initWithColors: newColors andWindow: newWindow
			enabled: NO];
}

//second init method - initializes the Attribute
//with the option to enable it
- (id) initWithColors: (ColorPair*) newColors
	andWindow: (Window*) newWindow enabled: (bool) newEnabled {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = ATTR_COLOR;
		_enabled = newEnabled;
		_colors = newColors;
		[_colors retain];
		_window = newWindow;
		[_window retain];

		//and determine whether to enable the Attribute
		if(_enabled) { //if the Attribute should be enabled
			//then enable it
			wattron([_window data], 
				COLOR_PAIR([_colors ID]));
		}
	}

	//and return the instance
	return self;
}

//copy method - copies the Attribute
- (id) copy {
	//return a copy of the Attribute
	return [[WindowColorAttribute alloc] initWithColors: [_colors copy]
						  andWindow: _window
						    enabled: _enabled];
}

//dealloc method - deallocates the Attribute
- (void) dealloc {
	[self disable]; //disable the Attribute
	[_colors release]; //release the colors
	[_window release]; //release the window
	[super dealloc]; //and call the superclass dealloc method
}

//enable method - enables the Attribute
- (void) enable {
	//determine whether the Attribute is currently enabled
	if(!_enabled) { //if the Attribute is not enabled
		//then enable it
		_enabled = YES;
		wattron([_window data], COLOR_PAIR([_colors ID]));
	}
}

//disable method - disables the Attribute
- (void) disable {
	//determine whether the Attribute is currently disabled
	if(_enabled) { //if the Attribute is not disabled
		//then disable it
		_enabled = NO;
		wattroff([_window data], COLOR_PAIR([_colors ID]));
	}
}

//combineWith method - combines the Attribute with another
- (void) combineWith: (id<Attribute>) other {
	//throw an exception
	ColorModificationException* exception =
		[ColorModificationException exception];

	@throw exception;
}

@end
