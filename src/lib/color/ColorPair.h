/*
 * ColorPair.h
 * Declares a class that represents a color pair
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "TextColor.h"

@interface ColorPair : NSObject {
	//fields
	TextColor* _foreground; //the foreground color
	TextColor* _background; //the background color
	int _ID; //the ID of the color pair
}

//property declarations
@property (nonatomic, readonly) TextColor* foreground;
@property (nonatomic, readonly) TextColor* background;
@property (readonly) int ID;

//method declarations

//initializes a ColorPair instance
- (id) initWithForeground: (TextColor*) newFore 
	andBackground: (TextColor*) newBackground andID: (int) newID;

//copies a ColorPair instance
- (id) copy;

//deallocates a ColorPair instance
- (void) dealloc;

@end 
