/*
 * ColorPair.m
 * Implements a class that represents a color pair
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "ColorPair.h"

//import the ncurses header
#import <ncurses.h>

@implementation ColorPair

//property synthesis
@synthesize foreground = _foreground;
@synthesize background = _background;
@synthesize ID = _ID;

//init method - initializes a ColorPair instance
- (id) initWithForeground: (TextColor*) newFore
	andBackground: (TextColor*) newBack andID: (int) newID {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_foreground = newFore;
		[_foreground retain];
		_background = newBack;
		[_background retain];
		_ID = newID;

		//and initialize the ncurses color pair
		init_pair(_ID, [_foreground type],
				[_background type]);
	}

	//and return the instance
	return self;
}

//copy method - copies a ColorPair instance
- (id) copy {
	//return a copy of the instance
	return [[ColorPair alloc] initWithForeground: [_foreground copy]
				       andBackground: [_background copy]
					       andID: _ID];
}

//dealloc method - deallocates a ColorPair instance
- (void) dealloc {
	[_foreground release]; //release the foreground color
	[_background release]; //release the background color
	[super dealloc]; //and call the superclass dealloc method
}

@end
