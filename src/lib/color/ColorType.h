/*
 * ColorType.h
 * Enumerates types of colors
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//no imports

//enum definition
typedef enum {
	COL_DEFAULT = -1,
	COL_BLACK,
	COL_RED,
	COL_GREEN,
	COL_YELLOW,
	COL_BLUE,
	COL_MAGENTA,
	COL_CYAN,
	COL_WHITE 
} ColorType;

//end of enum
