/*
 * TextColor.h
 * Declares a class that represents a text color
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "ColorType.h"

@interface TextColor : NSObject {
	//field
	ColorType _type; //the type of the color
}

//property declaration
@property (readwrite) ColorType type;

//method declarations

//initializes a TextColor instance with the default color
- (id) init;

//initializes a TextColor instance with a specific color type
- (id) initWithType: (ColorType) newType;

//copies a TextColor instance
- (id) copy;

//deallocates a TextColor instance
- (void) dealloc;

@end
