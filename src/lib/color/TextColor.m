/*
 * TextColor.m
 * Implements a class that represents a text color
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "TextColor.h"

@implementation TextColor

//property synthesis
@synthesize type = _type;

//first init method - initializes a TexxtColor instance 
//with the default type
- (id) init {
	//call the second init method
	return [self initWithType: COL_DEFAULT];
}

//second init method - initializes a TextColor instance 
//with a specified type
- (id) initWithType: (ColorType) newType {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_type = newType;
	}

	//and return the instance
	return self;
}

//copy method - copies a TextColor instance
- (id) copy {
	//return a copy of the instance
	return [[TextColor alloc] initWithType: _type];
}

//dealloc method - deallocates a TextColor instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

@end
