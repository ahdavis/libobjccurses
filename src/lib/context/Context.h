/*
 * Context.h
 * Declares a class that holds contextual data for libobjccurses
 * Created on 7/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//interface declaration
@interface Context : NSObject {
	//field
	NSArray* _enviroment; //the initialization enviroment
	bool _colorsEnabled; //are colors enabled?
	bool _suspended; //is curses mode suspended?
}

//property declarations
@property (nonatomic, readonly) NSArray* enviroment;
@property (readonly) bool colorsEnabled;
@property (readonly) bool suspended;

//method declarations

//initializes a Context instance with no special options enabled
- (id) init;

//initializes a Context instance with special options enabled
- (id) initWithEnviroment: (NSArray*) newEnviroment;

//called when a Context instance is deallocated
- (void) dealloc;

//refreshes the display screen
- (void) refreshScreen;

//returns the number of lines on the screen
- (int) lineCount;

//returns the number of columns on the screen
- (int) columnCount;

//returns the x-coord of the current cursor position on the screen
- (int) cursorX;

//returns the y-coord of the current cursor position on the screen
- (int) cursorY;

//clears the screen
- (void) clearScreen;

//makes the cursor invisible
- (void) makeCursorInvisible;

//makes the cursor visible
- (void) makeCursorVisible;

//makes the cursor very visible
- (void) makeCursorVeryVisible;

//suspends curses mode temporarily
- (void) suspendCursesMode;

//resumes curses mode
- (void) resumeCursesMode;

//logs a message to the console
- (void) logMessage: (NSString*) message;

@end
