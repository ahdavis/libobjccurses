/*
 * Context.m
 * Implements a class that holds contextual data for libobjccurses
 * Created on 7/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "Context.h"

//import the InitOption class
#import "InitOption.h"

//import the initialization option enum
#import "EnumInitOption.h"

//import the ncurses header
#import <ncurses.h>

//import C standard I/O
#import <stdio.h>

@implementation Context

//property synthesis
@synthesize enviroment = _enviroment;
@synthesize colorsEnabled = _colorsEnabled;
@synthesize suspended = _suspended;

//init method - initializes the Context with no special options
- (id) init {
	//call the initWithOptions method
	return [self initWithEnviroment: [[NSArray alloc] 
			initWithObjects: nil]];
}

//initWithEnviroment method - initializes the Context with special options
- (id) initWithEnviroment: (NSArray*) newEnviroment {
	//call the superclass init method
	self = [super init];

	//make sure that the call succeeded
	//if it did, then init the field
	if(self) { //if the call succeeded
		_enviroment = newEnviroment; //then init the options field
		[_enviroment retain]; //increment its retain count

		_colorsEnabled = NO; //init the colors enabled flag

		_suspended = NO; //init the suspended flag

		//initialize the ncurses state
		initscr();

		//enable mouse event listening for all mouse events
		mousemask(ALL_MOUSE_EVENTS, NULL);

		//call the necessary ncurses setup methods
		for(InitOption* opt in _enviroment) {
			//handle each option
			if(opt != nil) {
				switch([opt value]) {
					case INIT_RAW: 
						{
							raw();
							break;
						}
					case INIT_CBREAK:
						{
							cbreak();
							break;
						}
					case INIT_ECHO:
						{
							echo();
							break;
						}
					case INIT_NOECHO:
						{
							noecho();
							break;
						}
					case INIT_KEYPAD:
						{
							keypad(stdscr,
								TRUE);
							break;
						}
					case INIT_COLOR:
						{
							//verify that
							//colors are
							//supported
							if(has_colors()
							== FALSE) {
								endwin();
								puts(
							"Colors not \
							supported\n");
							exit(EXIT_FAILURE);
							} else {
							_colorsEnabled =
								YES;
							start_color();
						use_default_colors();
							}

							break;
						}
					default:
						{
							break;
						}
				}
			}
		}
	}

	//return the initialized object
	return self;
}

//dealloc method - deallocates a Context instance
- (void) dealloc {
	[_enviroment release]; //decrement the option field's retain count
	[self makeCursorVisible]; //make the cursor visible
	[super dealloc]; //call the superclass dealloc method
	endwin(); //and close out of ncurses mode
}

//refreshScreen method - refreshes the display screen
- (void) refreshScreen {
	refresh(); //call the ncurses refresh function
}

//lineCount method - returns the number of lines on the display screen
- (int) lineCount {
	return LINES; //return the line count
}

//columnCount method - returns the number of columns on the display screen
- (int) columnCount {
	return COLS; //return the column count
}

//cursorX method - returns the x-coord of the current cursor position
- (int) cursorX {
	int x, y; //will hold the cursor position
	getyx(stdscr, y, x); //populate the coordinates
	return x; //and return the x-coord
}

//cursorY method - returns the y-coord of the current cursor position
- (int) cursorY {
	int x, y; //will hold the cursor position
	getyx(stdscr, y, x); //populate the coordinates
	return y; //and return the y-coord
}

//clearScreen method - clears the screen
- (void) clearScreen {
	clear(); //clear the screen
}

//makeCursorInvisible method - makes the cursor invisible
- (void) makeCursorInvisible {
	curs_set(0); //make the cursor invisible
}

//makeCursorVisible method - makes the cursor visible
- (void) makeCursorVisible {
	curs_set(1); //make the cursor visible
}

//makeCursorVeryVisible method - makes the cursor very visible
- (void) makeCursorVeryVisible {
	curs_set(2); //make the cursor very visible
}

//suspendCursesMode method - suspends curses mode temporarily
- (void) suspendCursesMode {
	//determine whether curses mode is already suspended
	if(!_suspended) { //if curses mode is not suspended
		//then suspend it
		def_prog_mode();
		endwin();
		_suspended = YES;
	}
}

//resumeCursesMode method - resumes curses mode
- (void) resumeCursesMode {
	//determine whether curses mode is not currently suspended
	if(_suspended) { //if curses mode is suspended
		//then resume it
		reset_prog_mode();
		refresh();
		_suspended = NO;
	}
}

//logMessage method - logs a message to the console
- (void) logMessage: (NSString*) message {
	//save the current suspend flag
	bool wasSuspended = _suspended;

	//suspend curses mode
	[self suspendCursesMode];

	//log the message
	NSLog(@"%@", message);

	//and resume curses mode
	if(!wasSuspended) { //if ncurses was not suspended already
		//then resume it
		[self resumeCursesMode];
	}
}

@end //end of implementation
