/*
 * EnumInitOption.h
 * Enumerates initialization options for libobjccurses
 * Created on 7/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//no includes

//enum definition
typedef enum {
	INIT_RAW, //disable line buffering with no signal
	INIT_CBREAK, //disable line buffering with no interpretation
	INIT_ECHO, //enable character echoing
	INIT_NOECHO, //disable character echoing
	INIT_KEYPAD, //enable keypad input
	INIT_COLOR, //enable colors
} EnumInitOption;

//end of header
