/*
 * InitOption.h
 * Declares a class that represents an objccurses initialization option
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "EnumInitOption.h"

//interface declaration
@interface InitOption : NSObject {
	//field
	EnumInitOption _value; //the enum value that defines the option
}

//property declaration
@property (readonly) EnumInitOption value;

//method declarations

//instance methods

//initializes an InitOption instance
- (id) initWithValue: (EnumInitOption) newValue;

//copies an InitOption instance
- (id) copy;

//deallocates an InitOption instance
- (void) dealloc;

//class methods

//returns an InitOption instance initialized with the RAW option
+ (InitOption*) raw;

//returns an InitOption instance initialized with the CBREAK option
+ (InitOption*) cbreak;

//returns an InitOption instance initialized with the ECHO option
+ (InitOption*) echo;

//returns an InitOption instance initialized with the NOECHO option
+ (InitOption*) noecho;

//returns an InitOption instance initialized with the KEYPAD option
+ (InitOption*) keypad;

//returns an InitOption instance initialized with the COLOR option
+ (InitOption*) color;

@end
