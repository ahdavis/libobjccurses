/*
 * InitOption.m
 * Implements a class that represents an objccurses initialization option
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "InitOption.h"

@implementation InitOption

//synthesize the property
@synthesize value = _value;

//initWithValue method - initializes an InitOption instance
- (id) initWithValue: (EnumInitOption) newValue {
	//call the superclass init method
	self = [super init];

	//make sure that the call succeeded,
	//and if it did, then initialize the value field
	if(self) {
		_value = newValue; //init the value field
	}

	//return the initialized InitOption instance
	return self;
}

//copy method - copies an InitOption instance
- (id) copy {
	//copy the instance
	return [[InitOption alloc] initWithValue: _value];
}

//dealloc method - deallocates an InitOption instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//raw class method - returns an InitOption instance initialized 
//with the RAW option
+ (InitOption*) raw {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_RAW] autorelease];
}

//cbreak class method - returns an InitOption instance initialized 
//with the CBREAK option
+ (InitOption*) cbreak {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_CBREAK] 
			autorelease];
}

//echo class method - returns an InitOption instance initialized 
//with the ECHO option
+ (InitOption*) echo {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_ECHO] 
			autorelease];
}

//noecho class method - returns an InitOption instance initialized 
//with the NOECHO option
+ (InitOption*) noecho {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_NOECHO] 
			autorelease];
}

//keypad class method - returns an InitOption instance initialized 
//with the KEYPAD option
+ (InitOption*) keypad {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_KEYPAD] 
			autorelease];
}

//color class method - returns an InitOption instance initialized
//with the COLOR option
+ (InitOption*) color {
	//generate and return an InitOption instance
	return [[[InitOption alloc] initWithValue: INIT_COLOR] 
			autorelease];
}

@end 
