/*
 * ColorModificationException.h
 * Declares an exception that is thrown when a color attribute is modified
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

@interface ColorModificationException : NSException {
	//no fields
}

//no properties

//method declarations

//creates a ColorModificationException instance
+ (ColorModificationException*) exception;

//deallocates a ColorModificationException instance
- (void) dealloc;

@end
