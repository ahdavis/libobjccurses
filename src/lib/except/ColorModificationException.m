/*
 * ColorModificationException.m
 * Implements an exception that is thrown 
 * when a color attribute is modified
 * Created on 8/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "ColorModificationException.h"

//private methods
@interface ColorModificationException ()

//initializes a ColorModificationException instance
- (id) init;

@end 

@implementation ColorModificationException

//private init method - initializes an exception
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"ColorModificationException"
			    reason: @"Color attribute modified"
			  userInfo: nil];

	//and return the instance
	return self;
}

//exception class method - generates an exception instance
+ (ColorModificationException*) exception {
	//generate the exception
	return [[[ColorModificationException alloc] init] autorelease];
}

//dealloc method - deallocates an exception instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

@end
