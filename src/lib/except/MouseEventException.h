/*
 * MouseEventException.h
 * Declares an exception that is thrown when a mouse event does
 * not retrieve successfully
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

@interface MouseEventException : NSException {
	//no fields
}

//no properties

//method declarations

//returns an autoreleased MouseEventException instance
+ (MouseEventException*) exception;

//deallocates a MouseEventException instance
- (void) dealloc;

@end
