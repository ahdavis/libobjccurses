/*
 * MouseEventException.m
 * Implements an exception that is thrown when a mouse event does
 * not retrieve successfully
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MouseEventException.h"

//private methods
@interface MouseEventException ()

//initializes a MouseEventException instance
- (id) init;

@end

@implementation MouseEventException

//private init method - initializes a MouseEventException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"MouseEventException"
			reason: @"Failed to retrieve mouse event"
		      userInfo: nil];

	//and return the instance
	return self;
}

//exception class method - generates an autoreleased instance of the
//MouseEventException class
+ (MouseEventException*) exception {
	//return a MouseEventException instance
	return [[[MouseEventException alloc] init] autorelease];
}

//dealloc method - deallocates a MouseEventException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

@end 
