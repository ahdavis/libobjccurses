/*
 * AltChar.h
 * Declares a class that represents a member of the ncurses
 * alternate character set
 * Created on 8/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface AltChar : NSObject {
	//field
	int _data; //the actual character being represented
}

//property
@property (readonly) int data;

//method declarations

//returns the upper left corner character
+ (AltChar*) ULCORNER;

//returns the upper right corner character
+ (AltChar*) URCORNER;

//returns the lower left corner character
+ (AltChar*) LLCORNER;

//returns the upper left corner character
+ (AltChar*) ULCORNER;

//returns the right tee character
+ (AltChar*) RTEE;

//returns the left tee character
+ (AltChar*) LTEE;

//returns the up tee character
+ (AltChar*) TTEE;

//returns the down tee character
+ (AltChar*) BTEE;

//returns the horizontal line character
+ (AltChar*) HLINE;

//returns the vertical line character
+ (AltChar*) VLINE;

//returns the large plus character
+ (AltChar*) PLUS;

//returns the scan line 1 character
+ (AltChar*) S1;

//returns the scan line 3 character
+ (AltChar*) S3;

//returns the scan line 7 character
+ (AltChar*) S7;

//returns the scan line 9 character
+ (AltChar*) S9;

//returns the diamond character
+ (AltChar*) DIAMOND;

//returns the checkerboard character
+ (AltChar*) CKBOARD;

//returns the degree symbol
+ (AltChar*) DEGREE;

//returns the plus/minus symbol
+ (AltChar*) PLMINUS;

//returns the bullet symbol
+ (AltChar*) BULLET;

//returns the left arrow symbol
+ (AltChar*) LARROW;

//returns the right arrow symbol
+ (AltChar*) RARROW;

//returns the down arrow symbol
+ (AltChar*) DARROW;

//returns the up arrow symbol
+ (AltChar*) UARROW;

//returns the board of squares symbol
+ (AltChar*) BOARD;

//returns the lantern symbol
+ (AltChar*) LANTERN;

//returns the block symbol
+ (AltChar*) BLOCK;

//returns the less than or equal to sign
+ (AltChar*) LEQUAL;

//returns the greater than or equal to sign
+ (AltChar*) GEQUAL;

//returns the pi symbol
+ (AltChar*) PI;

//returns the not equal sign
+ (AltChar*) NEQUAL;

//returns the UK pound sign
+ (AltChar*) STERLING;

@end
