/*
 * AltChar.m
 * Implements a class that represents a member of the ncurses
 * alternate character set
 * Created on 8/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "AltChar.h"

//import the ncurses header
#import <ncurses.h>

//private methods
@interface AltChar ()

//initializes an AltChar instance
- (id) initWithSymbol: (int) sym;

@end

@implementation AltChar

//property synthesis
@synthesize data = _data;

//private init method - initializes an AltChar instance
- (id) initWithSymbol: (int) sym {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_data = sym;
	}

	//and return the instance
	return self;
}

//ULCORNER class method - generates an upper left corner symbol
+ (AltChar*) ULCORNER {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_ULCORNER]
		autorelease];
}

//URCORNER class method - generates an upper right corner symbol
+ (AltChar*) URCORNER {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_URCORNER]
		autorelease];
}

//LLCORNER class method - generates a lower left corner symbol
+ (AltChar*) LLCORNER {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LLCORNER]
		autorelease];
}

//LRCORNER class method - generates a lower right corner symbol
+ (AltChar*) LRCORNER {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LRCORNER]
		autorelease];
}

//LTEE class method - generates a left tee symbol
+ (AltChar*) LTEE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LTEE]
		autorelease];
}

//RTEE class method - generates a right tee symbol
+ (AltChar*) RTEE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_RTEE]
		autorelease];
}

//BTEE class method - generates a down tee symbol
+ (AltChar*) BTEE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_BTEE]
		autorelease];
}

//TTEE class method - generates an up tee symbol
+ (AltChar*) TTEE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_TTEE]
		autorelease];
}

//HLINE class method - generates a horizontal line symbol
+ (AltChar*) HLINE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_HLINE]
		autorelease];
}

//VLINE class method - generates a vertical line symbol
+ (AltChar*) VLINE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_VLINE]
		autorelease];
}

//PLUS class method - generates a large plus symbol
+ (AltChar*) PLUS {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_PLUS]
		autorelease];
}

//S1 class method - generates a scan line 1 symbol
+ (AltChar*) S1 {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_S1]
		autorelease];
}

//S3 class method - generates a scan line 3 symbol
+ (AltChar*) S3 {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_S3]
		autorelease];
}

//S7 class method - generates a scan line 7 symbol
+ (AltChar*) S7 {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_S7]
		autorelease];
}

//S9 class method - generates a scan line 9 symbol
+ (AltChar*) S9 {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_S9]
		autorelease];
}

//DIAMOND class method - generates a diamond symbol
+ (AltChar*) DIAMOND {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_DIAMOND]
		autorelease];
}

//CKBOARD class method - generates a checkerboard symbol
+ (AltChar*) CKBOARD {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_CKBOARD]
		autorelease];
}

//DEGREE class method - generates a degree symbol
+ (AltChar*) DEGREE {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_DEGREE]
		autorelease];
}

//PLMINUS class method - generates a plus/minus symbol
+ (AltChar*) PLMINUS {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_PLMINUS]
		autorelease];
}

//BULLET class method - generates a bullet symbol
+ (AltChar*) BULLET {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_BULLET]
		autorelease];
}

//LARROW class method - generates a left arrow symbol
+ (AltChar*) LARROW {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LARROW]
		autorelease];
}

//RARROW class method - generates a right arrow symbol
+ (AltChar*) RARROW {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_RARROW]
		autorelease];
}

//DARROW class method - generates a down arrow symbol
+ (AltChar*) DARROW {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_DARROW]
		autorelease];
}

//UARROW class method - generates an up arrow symbol
+ (AltChar*) UARROW {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_UARROW]
		autorelease];
}

//BOARD class method - generates a board of squares symbol
+ (AltChar*) BOARD {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_BOARD]
		autorelease];
}

//LANTERN class method - generates a lantern symbol
+ (AltChar*) LANTERN {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LANTERN]
		autorelease];
}

//BLOCK class method - generates a block symbol
+ (AltChar*) BLOCK {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_BLOCK]
		autorelease];
}

//LEQUAL class method - generates a less than or equal to symbol
+ (AltChar*) LEQUAL {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_LEQUAL]
		autorelease];
}

//GEQUAL class method - generates a greater than or equal to symbol
+ (AltChar*) GEQUAL {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_GEQUAL]
		autorelease];
}

//PI class method - generates a pi symbol
+ (AltChar*) PI {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_PI]
		autorelease];
}

//NEQUAL class method - generates a not equal symbol
+ (AltChar*) NEQUAL {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_NEQUAL]
		autorelease];
}

//STERLING class method - generates a British pound symbol
+ (AltChar*) STERLING {
	//return an AltChar instance
	return [[[AltChar alloc] initWithSymbol: ACS_STERLING]
		autorelease];
}

@end //end of implementation
