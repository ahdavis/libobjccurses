/*
 * InputScreenStream.h
 * Declares a class that reads data from the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "InputStream.h"

@interface InputScreenStream : NSObject <InputStream> {
	//no fields
}

//method declarations

//initializes an InputScreenStream
- (id) init;

//deallocates an InputScreenStream
- (void) dealloc;

//reads a string from the screen
- (NSString*) readString;

//reads a number from the screen
- (NSNumber*) readNumber;

//reads a character from the screen
- (int) readChar;

@end
