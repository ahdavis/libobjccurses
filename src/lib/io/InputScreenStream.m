/*
 * InputScreenStream.h
 * Declares a class that reads data from the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "InputScreenStream.h"

//import the ncurses header
#import <ncurses.h>

@implementation InputScreenStream

//init method - initializes an InputScreenStream instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//and return the instance
	return self;
}

//dealloc method - deallocates an InputScreenStream instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//readString method - reads a string from the screen
- (NSString*) readString {
	//declare a buffer to read into
	char buf[65536];

	//read into the buffer from the screen
	getnstr(buf, 65536);

	//and return a string created from the buffer
	return [NSString stringWithUTF8String: buf];
}

//readNumber method - reads a number from the screen
- (NSNumber*) readNumber {
	//read into a string
	NSString* str = [self readString];

	//create a number formatter to convert the string
	//to a number with
	NSNumberFormatter* nf = [[NSNumberFormatter alloc] init];

	//convert the string to a number
	NSNumber* num = [nf numberFromString: str];

	//determine whether the string had a number in it
	//if it did not, num will be nil
	if(num == nil) { //if the string did not have a number
		//then make the number 0
		num = [NSNumber numberWithInt: 0];
	}

	//deallocate the number formatter
	[nf release];

	//and return the number
	return num;
}

//readChar method - reads a character from the screen
- (int) readChar {
	return getch(); //return a character read from the screen
}

@end
