/*
 * InputStream.h
 * Declares a protocol for reading data from the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//protocol declaration
@protocol InputStream <NSObject>

//required methods
@required

//reads a string from the screen
- (NSString*) readString;

//reads a number from the screen
- (NSNumber*) readNumber;

//reads a character from the screen
- (int) readChar;

//optional methods
@optional

//returns the x-coord of the InputStream's read position
- (int) xPos;

//returns the y-coord of the InputStream's read position
- (int) yPos;

//sets the x-coord of the InputStream's read position
- (void) setXPos: (int) newXPos;

//sets the y-coord of the InputStream's read position
- (void) setYPos: (int) newYPos;


@end
