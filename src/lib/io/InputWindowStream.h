/*
 * InputWindowStream.h
 * Declares a class that reads data from a window
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "InputStream.h"
#import "../window/Window.h"

@interface InputWindowStream : NSObject <InputStream> {
	//field
	Window* _win; //the window to read from
}

//no properties

//method declarations

//initializes an InputWindowStream instance
- (id) initWithWindow: (Window*) newWindow;

//copies an InputWindowStream instance
- (id) copy;

//deallocates an InputWindowStream instance
- (void) dealloc;

//reads a string from the window
- (NSString*) readString;

//reads a number from the window
- (NSNumber*) readNumber;

//reads a character from the window
- (int) readChar;

@end
