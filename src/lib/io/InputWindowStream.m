/*
 * InputWindowStream.m
 * Implements a class that reads data from a window
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "InputWindowStream.h"

//import the ncurses header
#import <ncurses.h>

@implementation InputWindowStream

//init method - initializes an InputWindowStream instance
- (id) initWithWindow: (Window*) newWindow {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		_win = newWindow; //then init the window field
		[_win retain]; //and retain the window field
	}

	//return the instance
	return self;
}

//copy method - copies an InputWindowStream instance
- (id) copy {
	//return a copy of the InputWindowStream instance
	return [[InputWindowStream alloc] initWithWindow: [_win copy]];
}

//dealloc method - deallocates an InputWindowStream instance
- (void) dealloc {
	[_win release]; //release the window field
	[super dealloc]; //and call the superclass dealloc method
}

//readString method - reads a string from the window
- (NSString*) readString {
	//declare a buffer to read into
	char buf[65536];

	//read into the buffer from the window
	wgetnstr([_win data], buf, 65536);

	//and return a string created from the buffer
	return [NSString stringWithUTF8String: buf];
}

//readNumber method - reads a number from the window
- (NSNumber*) readNumber {
	//read into a string
	NSString* str = [self readString];

	//create a number formatter to convert the string
	//to a number with
	NSNumberFormatter* nf = [[NSNumberFormatter alloc] init];

	//convert the string to a number
	NSNumber* num = [nf numberFromString: str];

	//determine whether the string had a number in it
	//if it did not, num will be nil
	if(num == nil) { //if the string did not have a number
		//then make the number 0
		num = [NSNumber numberWithInt: 0];
	}

	//deallocate the number formatter
	[nf release];

	//and return the number
	return num;
}

//readChar method - reads a character from the window
- (int) readChar {
	//return a character read from the window
	return wgetch([_win data]);
}

@end
