/*
 * KeyCode.h
 * Declares a static class that holds keycodes
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

@interface KeyCode : NSObject {
	//no fields
}

//method declarations

//returns the keycode for a given function key
+ (int) codeForFunctionKey: (int) key;

//returns the keycode for the left arrow key
+ (int) leftArrow;

//returns the keycode for the right arrow key
+ (int) rightArrow;

//returns the keycode for the up arrow key
+ (int) upArrow;

//returns the keycode for the down arrow key
+ (int) downArrow;

//returns the "keycode" for a mouse interaction
+ (int) mouse;

@end
