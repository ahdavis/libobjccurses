/*
 * KeyCode.h
 * Declares a static class that holds keycodes
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "KeyCode.h"

//import the ncurses header
#import <ncurses.h>

@implementation KeyCode

//codeForFunctionKey method - returns the key code for a given function key
+ (int) codeForFunctionKey: (int) key {
	return KEY_F(key); //return the keycode
}

//leftArrow method - returns the key code for a left arrow key
+ (int) leftArrow {
	return KEY_LEFT;
}

//rightArrow method - returns the key code for a right arrow key
+ (int) rightArrow {
	return KEY_RIGHT;
}

//upArrow method - returns the key code for an up arrow key
+ (int) upArrow {
	return KEY_UP;
}

//downArrow method - returns the key code for a down arrow key
+ (int) downArrow {
	return KEY_DOWN;
}

//mouse method - returns the "key code" for mouse interaction
+ (int) mouse {
	return KEY_MOUSE;
}

@end
