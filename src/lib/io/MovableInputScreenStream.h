/*
 * MovableInputScreenStream.h
 * Declares a class that reads data from a screen in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "InputStream.h"
#import "InputScreenStream.h"

@interface MovableInputScreenStream : InputScreenStream
		<InputStream> {
	//fields
	int _xPos; //the current x-coord to read from
	int _yPos; //the current y-coord to read from
}

//property declarations
@property (readwrite) int xPos;
@property (readwrite) int yPos;

//method declarations

//initializes the stream at (0, 0)
- (id) init;

//initializes the stream at programmer-defined coordinates
- (id) initWithXPos: (int) newXPos andYPos: (int) newYPos;

//copies the stream
- (id) copy;

//deallocates the stream
- (void) dealloc;

//reads a string from the screen
- (NSString*) readString;

//reads a number from the screen
- (NSNumber*) readNumber;

//reads a character from the screen
- (int) readChar;

@end
