/*
 * MovableInputWindowStream.h
 * Declares a class that reads data from a window in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "InputStream.h"
#import "InputWindowStream.h"
#import "../window/Window.h"

@interface MovableInputWindowStream : InputWindowStream
		<InputStream> {
	//fields
	int _xPos; //the x-coord to read from
	int _yPos; //the y-coord to read from
}

//property declarations
@property (readwrite) int xPos;
@property (readwrite) int yPos;

//method declarations

//initializes the stream at (0, 0)
- (id) initWithWindow: (Window*) newWindow;

//initializes the stream with programmer-defined coordinates
- (id) initWithWindow: (Window*) newWindow andXPos: (int) newXPos
	andYPos: (int) newYPos;

//copies the stream
- (id) copy;

//deallocates the stream
- (void) dealloc;

//reads a string from the window
- (NSString*) readString;

//reads a number from the window
- (NSNumber*) readNumber;

//reads a character from the window
- (int) readChar;

@end 
