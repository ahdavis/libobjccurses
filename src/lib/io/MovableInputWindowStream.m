/*
 * MovableInputWindowStream.m
 * Implements a class that reads data from a window in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MovableInputWindowStream.h"

//import the ncurses header
#import <ncurses.h>

@implementation MovableInputWindowStream

//property synthesis
@synthesize xPos = _xPos;
@synthesize yPos = _yPos;

//first init method - initializes the stream at (0, 0)
- (id) initWithWindow: (Window*) newWindow {
	//call the second init method
	return [self initWithWindow: newWindow andXPos: 0 andYPos: 0];
}

//second init method - initializes the stream
//at programmer-specified coordinates
- (id) initWithWindow: (Window*) newWindow andXPos: (int) newXPos
	andYPos: (int) newYPos {
	//call the superclass init method
	self = [super initWithWindow: newWindow];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_xPos = newXPos;
		_yPos = newYPos;
	}

	//and return the instance
	return self;
}

//copy method - copies the stream
- (id) copy {
	//return a copy of the stream
	return [[MovableInputWindowStream alloc]
		initWithWindow: [_win copy] andXPos: _xPos andYPos: _yPos];
}

//dealloc method - deallocates the stream
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//readString method - reads a string from the window
- (NSString*) readString {
	//declare a buffer to read into
	char buf[65536];

	//read into the buffer from the window
	mvwgetnstr([_win data], _yPos, _xPos, buf, 65536);

	//and return a string created from the buffer
	return [NSString stringWithUTF8String: buf];
}

//readNumber method - reads a number from the window
- (NSNumber*) readNumber {
	//read into a string
	NSString* str = [self readString];

	//create a number formatter to convert the string
	//to a number with
	NSNumberFormatter* nf = [[NSNumberFormatter alloc] init];

	//convert the string to a number
	NSNumber* num = [nf numberFromString: str];

	//determine whether the string had a number in it
	//if it did not, num will be nil
	if(num == nil) { //if the string did not have a number
		//then make the number 0
		num = [NSNumber numberWithInt: 0];
	}

	//deallocate the number formatter
	[nf release];

	//and return the number
	return num;
}

//readChar method - reads a character from the window
- (int) readChar {
	//return a character read from the window
	return mvwgetch([_win data], _yPos, _xPos);
}

@end 
