/*
 * MovableOutputScreenStream.h
 * Declares a class that writes data to a screen in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "OutputStream.h"
#import "OutputScreenStream.h"
#import "AltChar.h"

@interface MovableOutputScreenStream : OutputScreenStream 
		<OutputStream> {
	//fields
	int _xPos; //the x-coord to write to
	int _yPos; //the y-coord to write to
}

//property declarations
@property (readwrite) int xPos;
@property (readwrite) int yPos;

//method declarations

//initializes a MovableOutputScreenStream at (0, 0)
- (id) init;

//initializes a MovableOutputScreenStream at programmer-specified coords
- (id) initWithXPos: (int) newXPos andYPos: (int) newYPos;

//copies a MovableOutputScreenStream instance
- (id) copy;

//deallocates a MovableOutputScreenStream instance
- (void) dealloc;

//writes a string to the screen
- (void) writeString: (NSString*) str;

//writes a number to the screen
- (void) writeNumber: (NSNumber*) num;

//writes a character to the screen
- (void) writeChar: (int) ch;

//writes an ACS character to the screen
- (void) writeAltChar: (AltChar*) ch;

@end
