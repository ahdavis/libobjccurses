/*
 * MovableOutputScreenStream.m
 * Implements a class that writes data to a screen in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MovableOutputScreenStream.h"

//import the ncurses header
#import <ncurses.h>

//import the attribute headers
#import "../attr/AttributeType.h"
#import "../attr/ScreenAttribute.h"

@implementation MovableOutputScreenStream

//property synthesis
@synthesize xPos = _xPos;
@synthesize yPos = _yPos;

//first init method - initializes the stream at (0, 0)
- (id) init {
	//call the second init method
	return [self initWithXPos: 0 andYPos: 0];
}

//second init method - initializes the stream at programmer-defined coords
- (id) initWithXPos: (int) newXPos andYPos: (int) newYPos {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_xPos = newXPos;
		_yPos = newYPos;
	}

	//and return the instance
	return self;
}

//copy method - copies the stream
- (id) copy {
	//return a copy of the stream
	return [[MovableOutputScreenStream alloc] initWithXPos: _xPos
			andYPos: _yPos];
}

//dealloc method - deallocates the stream
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//writeString method - writes a string to the screen
- (void) writeString: (NSString*) str {
	//write the string to the screen
	mvprintw(_yPos, _xPos, [str UTF8String]);
}

//writeNumber method - writes a number to the screen
- (void) writeNumber: (NSNumber*) num {
	//cast the number to a string and write it to the screen
	[self writeString: [num stringValue]];
}

//writeChar method - writes a character to the screen
- (void) writeChar: (int) ch {
	mvaddch(_yPos, _xPos, ch); //write the character to the screen
}

//writeAltChar method - writes an ACS character to the screen
- (void) writeAltChar: (AltChar*) ch {
	//get an attribute to enable ACS characters with
	ScreenAttribute* acs = [[ScreenAttribute alloc]
					initWithType: ATTR_ACS];

	//enable it
	[acs enable];

	//write the ACS character to the screen
	[self writeChar: [ch data]];

	//disable the attribute
	[acs disable];

	//and release the attribute
	[acs release];
}

@end
