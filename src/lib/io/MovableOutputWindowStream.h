/*
 * MovableOutputWindowStream.h
 * Declares a class that writes data to a window in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "OutputStream.h"
#import "OutputWindowStream.h"
#import "../window/Window.h"
#import "AltChar.h"

@interface MovableOutputWindowStream : OutputWindowStream
		<OutputStream> {
	//fields
	int _xPos; //the x-coord to write to
	int _yPos; //the y-coord to write to
}

//property declarations
@property (readwrite) int xPos;
@property (readwrite) int yPos;

//method declarations

//initializes the stream at (0, 0)
- (id) initWithWindow: (Window*) newWindow;

//initializes the stream at programmer-specified coordinates
- (id) initWithWindow: (Window*) newWindow andXPos: (int) newXPos
	andYPos: (int) newYPos;

//copies the stream
- (id) copy;

//deallocates the stream
- (void) dealloc;

//writes a string to the window
- (void) writeString: (NSString*) str;

//writes a number to the window
- (void) writeNumber: (NSNumber*) num;

//writes a character to the window
- (void) writeChar: (int) ch;

//writes an ACS character to the window
- (void) writeAltChar: (AltChar*) ch;

@end 
