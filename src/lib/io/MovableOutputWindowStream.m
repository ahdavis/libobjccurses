/*
 * MovableOutputWindowStream.m
 * Implements a class that writes data to a window in different locations
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MovableOutputWindowStream.h"

//import the ncurses header
#import <ncurses.h>

//import the attribute headers
#import "../attr/AttributeType.h"
#import "../attr/WindowAttribute.h"

@implementation MovableOutputWindowStream

//property synthesis
@synthesize xPos = _xPos;
@synthesize yPos = _yPos;

//first init method - initializes the stream at (0, 0)
- (id) initWithWindow: (Window*) newWindow {
	//call the second init method
	return [self initWithWindow: newWindow andXPos: 0 andYPos: 0];
}

//second init method - initializes the stream at 
//programmer-specified coordinates
- (id) initWithWindow: (Window*) newWindow andXPos: (int) newXPos
	andYPos: (int) newYPos {
	//call the superclass init method
	self = [super initWithWindow: newWindow];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then initialize the fields
		_xPos = newXPos;
		_yPos = newYPos;
	}

	//and return the instance
	return self;
}

//copy method - copies the stream
- (id) copy {
	//return a copy of the stream
	return [[MovableOutputWindowStream alloc]
		initWithWindow: [_win copy] andXPos: _xPos andYPos: _yPos];
}

//dealloc method - deallocates the stream
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//writeString method - writes a string to the window
- (void) writeString: (NSString*) str {
	//write the string to the window
	mvwprintw([_win data], _yPos, _xPos, [str UTF8String]);
}

//writeNumber method - writes a number to the window
- (void) writeNumber: (NSNumber*) num {
	//write the number to the window
	[self writeString: [num stringValue]];
}

//writeChar method - writes a character to the window
- (void) writeChar: (int) ch {
	//write the character to the window
	mvwaddch([_win data], _yPos, _xPos, ch);
}

//writeAltChar method - writes an ACS character to the window
- (void) writeAltChar: (AltChar*) ch {
	//get an attribute to enable ACS with
	WindowAttribute* acs = [[WindowAttribute alloc] 
				initWithWindow: _win andType: ATTR_ACS];

	//enable it
	[acs enable];

	//write the character to the window
	[self writeChar: [ch data]];

	//disable the attribute
	[acs disable];

	//and release the attribute
	[acs release];
}

@end 
