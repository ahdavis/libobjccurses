/*
 * OutputScreenStream.h
 * Declares a class that writes data to the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "OutputStream.h"
#import "AltChar.h"

//class declaration
@interface OutputScreenStream : NSObject <OutputStream> {
	//no fields
}

//method declarations

//initializes an OutputScreenStream instance
- (id) init;

//deallocates an OutputScreenStream instance
- (void) dealloc;

//writes a string to the screen
- (void) writeString: (NSString*) str;

//writes a number to the screen
- (void) writeNumber: (NSNumber*) num;

//writes a character to the screen
- (void) writeChar: (int) ch;

//writes an ACS character to the screen
- (void) writeAltChar: (AltChar*) ch;

@end
