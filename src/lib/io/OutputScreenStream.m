/*
 * OutputScreenStream.m
 * Implements a class that writes data to the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "OutputScreenStream.h"

//import the ncurses header
#import <ncurses.h>

//import the attribute headers
#import "../attr/ScreenAttribute.h"
#import "../attr/AttributeType.h"

@implementation OutputScreenStream

//init method - initializes an OutputScreenStream instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//and return the instance
	return self;
}

//dealloc method - deallocates an OutputScreenStream instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//writeString method - writes a string to the screen
- (void) writeString: (NSString*) str {
	//get a C string from the argument string
	const char* cstr = [str UTF8String];

	//and write the C string to the screen
	printw(cstr);
}

//writeNumber method - writes a number to the screen
- (void) writeNumber: (NSNumber*) num {
	//cast the number to a string and write it to the screem
	[self writeString: [num stringValue]];
}

//writeChar method - writes a character to the screen
- (void) writeChar: (int) ch {
	addch(ch); //write the character to the screen
}

//writeAltChar method - writes an ACS character to the screen
- (void) writeAltChar: (AltChar*) ch {
	//create an attribute to enable ACS characters
	ScreenAttribute* acs = [[ScreenAttribute alloc] 
					initWithType: ATTR_ACS];
	
	//enable the attribute
	[acs enable];

	//write the ACS character to the screen
	[self writeChar: [ch data]];

	//disable the attribute
	[acs disable];

	//and release the attribute
	[acs release];
}

@end
