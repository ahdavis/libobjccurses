/*
 * OutputStream.h
 * Declares a protocol for writing data to the screen
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "AltChar.h"

//protocol declaration
@protocol OutputStream <NSObject>

//required methods
@required 

//writes a string to the screen
- (void) writeString: (NSString*) str;

//writes a number to the screen
- (void) writeNumber: (NSNumber*) num;

//writes a character to the screen
- (void) writeChar: (int) ch;

//writes an ACS character to the screen
- (void) writeAltChar: (AltChar*) ch;

//optional methods
@optional

//returns the x-coord of the OutputStream's write position
- (int) xPos;

//returns the y-coord of the OutputStream's write position
- (int) yPos;

//sets the x-coord of the OutputStream's write position
- (void) setXPos: (int) newXPos;

//sets the y-coord of the OutputStream's write position
- (void) setYPos: (int) newYPos;

@end
