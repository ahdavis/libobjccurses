/*
 * OutputWindowStream.h
 * Declares a class that writes data to a window
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "OutputStream.h"
#import "../window/Window.h"
#import "AltChar.h"

@interface OutputWindowStream : NSObject <OutputStream> {
	//field
	Window* _win; //the window to write to
}

//no properties

//method declarations

//initializes an OutputWindowStream instance
- (id) initWithWindow: (Window*) newWindow;

//deallocates an OutputWindowStream instance
- (void) dealloc;

//copies an OutputWindowStream instance
- (id) copy;

//writes a string to the window
- (void) writeString: (NSString*) str;

//writes a number to the window
- (void) writeNumber: (NSNumber*) num;

//writes a character to the window
- (void) writeChar: (int) ch;

//writes an ACS character to the window
- (void) writeAltChar: (AltChar*) ch;

@end
