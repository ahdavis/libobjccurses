/*
 * OutputWindowStream.m
 * Implements a class that writes data to a window
 * Created on 8/6/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "OutputWindowStream.h"

//import the ncurses header
#import <ncurses.h>

//import the attribute headers
#import "../attr/AttributeType.h"
#import "../attr/WindowAttribute.h"

@implementation OutputWindowStream

//init method - initializes an OutputWindowStream instance
- (id) initWithWindow: (Window*) newWindow {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		_win = newWindow; //then initialize the window field
		[_win retain]; //and retain the window field
	}

	//return the instance
	return self;
}

//dealloc method - deallocates an OutputWindowStream instance
- (void) dealloc {
	[_win release]; //release the window instance
	[super dealloc]; //and call the superclass dealloc method
}

//copy method - copies an OutputWindowStream instance
- (id) copy {
	//return a copy of the instance
	return [[OutputWindowStream alloc] initWithWindow: [_win copy]];
}

//writeString method - writes a string to the window
- (void) writeString: (NSString*) str {
	wprintw([_win data], [str UTF8String]); //write the string
	[_win refresh]; //and refresh the window
}

//writeNumber method - writes a number to the window
- (void) writeNumber: (NSNumber*) num {
	//call the writeString method
	[self writeString: [num stringValue]];
}

//writeChar method - writes a character to the window
- (void) writeChar: (int) ch {
	waddch([_win data], ch); //write the character
	[_win refresh]; //and refresh the window
}

//writeAltChar method - writes an ACS character to the window
- (void) writeAltChar: (AltChar*) ch {
	//get an attribute to enable ACS
	WindowAttribute* acs = [[WindowAttribute alloc] 
				initWithWindow: _win andType: ATTR_ACS];

	//enable ACS
	[acs enable];

	//write the character to the window
	[self writeChar: [ch data]];

	//disable ACS
	[acs disable];

	//and release the attribute
	[acs release];
}

@end
