/*
 * MouseEvent.h
 * Declares a class that represents a mouse event
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "MouseMask.h"

@interface MouseEvent : NSObject {
	//fields
	int _ID; //the ID of the triggering device
	int _x; //the x-coord of the mouse
	int _y; //the y-coord of the mouse
	int _z; //the z-coord of the mouse
	int _state; //the button state bits
}

//property declarations
@property (readonly) int ID;
@property (readonly) int x;
@property (readonly) int y;
@property (readonly) int z;

//method declarations

//initializes a MouseEvent instance
- (id) initWithID: (int) newID andX: (int) newX andY: (int) newY
	andZ: (int) newZ andState: (int) newState;

//copies a MouseEvent instance
- (id) copy;

//deallocates a MouseEvent instance
- (void) dealloc;

//returns whether the MouseEvent was triggered by a specified
//mouse action
- (bool) triggeredBy: (MouseMask) action;

@end
