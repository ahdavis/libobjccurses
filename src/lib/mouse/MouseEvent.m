/*
 * MouseEvent.m
 * Implements a class that represents a mouse event
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MouseEvent.h"

@implementation MouseEvent

//property synthesis
@synthesize ID = _ID;
@synthesize x = _x;
@synthesize y = _y;
@synthesize z = _z;

//init method - initializes a MouseEvent
- (id) initWithID: (int) newID andX: (int) newX andY: (int) newY
	andZ: (int) newZ andState: (int) newState {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_ID = newID;
		_x = newX;
		_y = newY;
		_z = newZ;
		_state = newState;
	}

	//and return the instance
	return self;
}

//copy method - copies a MouseEvent instance
- (id) copy {
	//return a copy of the instance
	return [[MouseEvent alloc] initWithID: _ID andX: _x andY: _y
			andZ: _z andState: _state];
}

//dealloc method - deallocates the MouseEvent instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//triggeredBy method - returns whether the event was triggered by
//a specified action
- (bool) triggeredBy: (MouseMask) action {
	//verify the trigger with a bitwise AND
	return ((_state & action) == 0) ? NO : YES; 
}

@end
