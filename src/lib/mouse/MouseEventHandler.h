/*
 * MouseEventHandler.h
 * Declares a class that handles mouse events
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "MouseEvent.h"
#import "MouseMask.h"

@interface MouseEventHandler : NSObject {
	//field
	MouseMask _eventMask; //the event mask
}

//property declaration
@property (readonly) MouseMask eventMask;

//method declarations

//initializes a MouseEventHandler instance with all events enabled
- (id) init;

//initializes a MouseEventHandler instance with a specific event enabled
- (id) initWithMask: (MouseMask) newMask;

//deallocates a MouseEventHandler instance
- (void) dealloc;

//retrieves the next mouse event from the event queue and returns it
//throws a MouseEventException if the retrieval fails
- (MouseEvent*) pollEvent;

//sets the mouse event mask
+ (void) setMouseMask: (MouseMask) newMask;

@end 
