/*
 * MouseEventHandler.m
 * Implements a class that handles mouse events
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "MouseEventHandler.h"

//import the ncurses header
#import <ncurses.h>

//import the mouse event exception class
#import "../except/MouseEventException.h"

@implementation MouseEventHandler

//property synthesis
@synthesize eventMask = _eventMask;

//first init method - initializes a MouseEventHandler instance
//with all events enabled
- (id) init {
	//call the second init method
	return [self initWithMask: ALL_EVENTS];
}

//second init method - initializes a MouseEventHandler instance
//with a specified event enabled
- (id) initWithMask: (MouseMask) newMask {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_eventMask = newMask;

		//and set the global event mask
		[MouseEventHandler setMouseMask: _eventMask];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a MouseEventHandler instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//pollEvent - retrieves the next mouse event from the event queue
- (MouseEvent*) pollEvent {
	//declare a variable to hold the event data
	MEVENT data;

	//zero out the event data variable
	memset(&data, 0, sizeof(MEVENT));

	//get the mouse event
	int res = getmouse(&data);

	//verify that the retrieval succeeded
	if(res == ERR) { //if the retrieval failed
		//then throw an exception
		MouseEventException* exception =
			[MouseEventException exception];
		@throw exception;
	} 

	//if control reaches here, then the retrieval succeeded
	//so return a MouseEvent instance
	return [[[MouseEvent alloc] initWithID: data.id andX: data.x
			andY: data.y andZ: data.z andState: data.bstate]
				autorelease];
}

//setMouseMask class method - sets the mouse mask
+ (void) setMouseMask: (MouseMask) newMask {
	mousemask(newMask, NULL); //set the mouse mask
}

@end 
