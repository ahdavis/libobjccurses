/*
 * MouseMask.h
 * Enumerates mouse input values
 * Created on 8/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//include
#include <ncurses.h>

//enum definition
typedef enum {
	B1_PRESSED = BUTTON1_PRESSED, //mouse button 1 pressed
	B1_RELEASED = BUTTON1_RELEASED, //mouse button 1 released
	B1_CLICKED = BUTTON1_CLICKED, //mouse button 1 clicked
	B1_DOUBLE_CLICKED = BUTTON1_DOUBLE_CLICKED, //B1 double clicked
	B1_TRIPLE_CLICKED = BUTTON1_TRIPLE_CLICKED, //B1 triple clicked
	B2_PRESSED = BUTTON2_PRESSED, //button 2 pressed
	B2_RELEASED = BUTTON2_RELEASED, //button 2 released
	B2_CLICKED = BUTTON2_CLICKED, //button 2 clicked
	B2_DOUBLE_CLICKED = BUTTON2_DOUBLE_CLICKED, //B2 double clicked
	B2_TRIPLE_CLICKED = BUTTON2_TRIPLE_CLICKED, //B2 triple clicked
	B3_PRESSED = BUTTON3_PRESSED, //mouse button 1 pressed
	B3_RELEASED = BUTTON3_RELEASED, //mouse button 1 released
	B3_CLICKED = BUTTON3_CLICKED, //mouse button 1 clicked
	B3_DOUBLE_CLICKED = BUTTON3_DOUBLE_CLICKED, //B1 double clicked
	B3_TRIPLE_CLICKED = BUTTON3_TRIPLE_CLICKED, //B1 triple clicked
	B4_PRESSED = BUTTON4_PRESSED, //button 2 pressed
	B4_RELEASED = BUTTON4_RELEASED, //button 2 released
	B4_CLICKED = BUTTON4_CLICKED, //button 2 clicked
	B4_DOUBLE_CLICKED = BUTTON4_DOUBLE_CLICKED, //B2 double clicked
	B4_TRIPLE_CLICKED = BUTTON4_TRIPLE_CLICKED, //B2 triple clicked
	SHIFT_DOWN = BUTTON_SHIFT, //shift down during event
	CTRL_DOWN = BUTTON_CTRL, //control down during event
	ALT_DOWN = BUTTON_ALT, //alt down during event
	ALL_EVENTS = ALL_MOUSE_EVENTS, //all mouse events
	MOUSE_MOVED = REPORT_MOUSE_POSITION //mouse moved
} MouseMask;

//end of enum
