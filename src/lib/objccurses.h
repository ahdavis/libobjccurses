/*
 * objccurses.h
 * Master include file for libobjccurses
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import "context/Context.h"
#import "context/InitOption.h"
#import "io/InputStream.h"
#import "io/OutputStream.h"
#import "io/InputScreenStream.h"
#import "io/OutputScreenStream.h"
#import "io/KeyCode.h"
#import "io/OutputWindowStream.h"
#import "io/InputWindowStream.h"
#import "io/MovableOutputScreenStream.h"
#import "io/MovableInputScreenStream.h"
#import "io/MovableOutputWindowStream.h"
#import "io/MovableInputWindowStream.h"
#import "io/AltChar.h"
#import "window/Border.h"
#import "window/Window.h"
#import "attr/Attribute.h"
#import "attr/AttributeType.h"
#import "attr/ScreenAttribute.h"
#import "attr/WindowAttribute.h"
#import "attr/ScreenColorAttribute.h"
#import "attr/WindowColorAttribute.h"
#import "color/TextColor.h"
#import "color/ColorPair.h"
#import "color/ColorType.h"
#import "except/ColorModificationException.h"
#import "except/MouseEventException.h"
#import "mouse/MouseEvent.h"
#import "mouse/MouseEventHandler.h"
#import "mouse/MouseMask.h"

//end of header
