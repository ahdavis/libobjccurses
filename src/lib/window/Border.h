/*
 * Border.h
 * Declares a class that represents a window border
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

@interface Border : NSObject {
	//fields
	char _leftSide; //the left side of the border
	char _rightSide; //the right side of the border
	char _top; //the top of the border
	char _bottom; //the bottom of the border
	char _topLeft; //the top left corner of the border
	char _topRight; //the top right corner of the border
	char _bottomLeft; //the bottom left corner of the border
	char _bottomRight; //the bottom right corner of the border
}

//properties
@property (readwrite) char leftSide;
@property (readwrite) char rightSide;
@property (readwrite) char top;
@property (readwrite) char bottom;
@property (readwrite) char topLeft;
@property (readwrite) char topRight;
@property (readwrite) char bottomLeft;
@property (readwrite) char bottomRight;

//method declarations

//initializes a Border with spaces for each character
- (id) init;

//initializes a Border with specified characters
- (id) initWithLeftSide: (char) newLeft andRightSide: (char) newRight
	andTop: (char) newTop andBottom: (char) newBottom
	andTopLeft: (char) newTopLeft andTopRight: (char) newTopRight
	andBottomLeft: (char) newBottomLeft 
	andBottomRight: (char) newBottomRight;

//initializes a Border with the default ncurses characters
- (id) initWithDefaults;

//initializes a Border with the default corner characters and
//programmer-specified side characters
- (id) initWithLeftSide: (char) newLeft andRightSide: (char) newRight
	andTop: (char) newTop andBottom: (char) newBottom;

//initializes a Border with the default side characters and
//programmer-specified corner characters
- (id) initWithTopLeft: (char) newTopLeft andTopRight: (char) newTopRight
	andBottomLeft: (char) newBottomLeft 
	andBottomRight: (char) newBottomRight;

//copies a Border
- (id) copy;

//deallocates a Border
- (void) dealloc;

@end
