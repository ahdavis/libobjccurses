/*
 * Border.m
 * Implements a class that represents a window border
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "Border.h"

//import the ncurses header
#import <ncurses.h>

@implementation Border

//property synthesis
@synthesize leftSide = _leftSide;
@synthesize rightSide = _rightSide;
@synthesize top = _top;
@synthesize bottom = _bottom;
@synthesize topLeft = _topLeft;
@synthesize topRight = _topRight;
@synthesize bottomLeft = _bottomLeft;
@synthesize bottomRight = _bottomRight;

//first init method - initializes a Border with spaces for each character
- (id) init {
	//call the second init method
	return [self initWithLeftSide: ' ' andRightSide: ' '
			       andTop: ' ' andBottom: ' '
			   andTopLeft: ' ' andTopRight: ' '
			andBottomLeft: ' ' andBottomRight: ' '];
}

//second init method - initializes a Border with specified characters
- (id) initWithLeftSide: (char) newLeft andRightSide: (char) newRight
	andTop: (char) newTop andBottom: (char) newBottom
    andTopLeft: (char) newTopLeft andTopRight: (char) newTopRight
 andBottomLeft: (char) newBottomLeft 
andBottomRight: (char) newBottomRight {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_leftSide = newLeft;
		_rightSide = newRight;
		_top = newTop;
		_bottom = newBottom;
		_topLeft = newTopLeft;
		_topRight = newTopRight;
		_bottomLeft = newBottomLeft;
		_bottomRight = newBottomRight;
	}

	//and return the instance
	return self;
}

//third init method - initializes a Border with default ncurses characters
- (id) initWithDefaults {
	//call the second init method
	return [self initWithLeftSide: ACS_VLINE andRightSide: ACS_VLINE
			       andTop: ACS_HLINE andBottom: ACS_HLINE
			   andTopLeft: ACS_ULCORNER
			  andTopRight: ACS_URCORNER
			andBottomLeft: ACS_LLCORNER
		       andBottomRight: ACS_LRCORNER];
}

//fourth init method - initializes a Border with default corner characters
//and programmer-specified side characters
- (id) initWithLeftSide: (char) newLeft andRightSide: (char) newRight
		 andTop: (char) newTop andBottom: (char) newBottom {
	//call the second init method
	return [self initWithLeftSide: newLeft andRightSide: newRight
			andTop: newTop andBottom: newBottom
		    andTopLeft: ACS_ULCORNER
		    andTopRight: ACS_URCORNER
		 andBottomLeft: ACS_LLCORNER
		andBottomRight: ACS_URCORNER];
}

//fifth init method - initializes a Border with default side characters
//and programmer-specified corner characters
- (id) initWithTopLeft: (char) newTopLeft andTopRight: (char) newTopRight
		andBottomLeft: (char) newBottomLeft
	       andBottomRight: (char) newBottomRight {
	//call the second init method
	return [self initWithLeftSide: ACS_VLINE andRightSide: ACS_VLINE
			andTop: ACS_HLINE andBottom: ACS_HLINE
		    andTopLeft: newTopLeft
		   andTopRight: newTopRight
		 andBottomLeft: newBottomLeft
		andBottomRight: newBottomRight];
}

//copy method - copies a Border
- (id) copy {
	//return a copy of the border
	return [[Border alloc] initWithLeftSide: [self leftSide]
			andRightSide: [self rightSide]
			      andTop: [self top]
			   andBottom: [self bottom]
			  andTopLeft: [self topLeft]
			 andTopRight: [self topRight]
		       andBottomLeft: [self bottomLeft]
		      andBottomRight: [self bottomRight]];
}

//dealloc method - deallocates a Border
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

@end
