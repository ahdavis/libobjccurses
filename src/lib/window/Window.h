/*
 * Window.h
 * Declares a class that represents a window
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <ncurses.h>
#import "Border.h"

@interface Window : NSObject {
	//fields
	WINDOW* _data; //the actual window data
	Border* _border; //the window border
	int _x; //the x-coord of the window
	int _y; //the y-coord of the window
	int _width; //the width of the window
	int _height; //the height of the window
}

//property declarations
@property (readonly) WINDOW* data;
@property (nonatomic, readonly) Border* border;
@property (nonatomic) int x;
@property (nonatomic) int y;
@property (nonatomic) int width;
@property (nonatomic) int height;

//method declarations

//initializes a Window instance at (0, 0) with a specified width and height
//and no border
- (id) initWithWidth: (int) newWidth andHeight: (int) newHeight;

//initializes a Window instance with specified coordinates and dimensions
//and no border
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight;

//initializes a Window instance at (0, 0) with a specified width,
//height, and border
- (id) initWithWidth: (int) newWidth andHeight: (int) newHeight
	andBorder: (Border*) newBorder;

//initializes a Window instance with specified coordinates, dimensions,
//and a border
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight andBorder: (Border*) newBorder;

//copies a Window instance
- (id) copy;

//deallocates a Window instance
- (void) dealloc;

//returns the maximum x-value of the Window
- (int) maxX;

//returns the maximum y-value of the Window
- (int) maxY;

//returns the current x-coord of the cursor in the Window
- (int) cursorX;

//returns the current y-coord of the cursor in the Window
- (int) cursorY;

//sets the window border
- (void) setBorder: (Border*) newBorder;

//sets the x-coord of the Window
- (void) setX: (int) newX;

//sets the y-coord of the Window
- (void) setY: (int) newY;

//sets the width of the Window
- (void) setWidth: (int) newWidth;

//sets the height of the Window
- (void) setHeight: (int) newHeight;

//refreshes the Window
- (void) refresh;

//removes the Window's border
- (void) removeBorder;

//enables keypad entry for the Window
- (void) enableKeypad;

//disables keypad entry for the Window
- (void) disableKeypad;

//clears the Window
- (void) clear;

@end
