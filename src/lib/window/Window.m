/*
 * Window.m
 * Implements a class that represents a window
 * Created on 8/3/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the header
#import "Window.h"

//private methods
@interface Window ()

//creates the window data
- (void) create;

//destroys the window data
- (void) destroy;

//refreshes the border
- (void) refreshBorder;

@end

@implementation Window

//property synthesis
@synthesize data = _data;
@synthesize border = _border;
@synthesize x = _x;
@synthesize y = _y;
@synthesize width = _width;
@synthesize height = _height;

//first init method - initializes a Window at (0, 0) with specified
//width and height and no border
- (id) initWithWidth: (int) newWidth andHeight: (int) newHeight {
	//call the third init method
	return [self initWithWidth: newWidth andHeight: newHeight
			andBorder: nil];
}

//second init method - initializes a Window with specified coordinates
//and dimensions and no border
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight {
	//call the fourth init method
	return [self initWithX: newX andY: newY andWidth: newWidth
		     andHeight: newHeight andBorder: nil];
}

//third init method - initializes a Window at (0, 0) with specified
//width, height, and a border
- (id) initWithWidth: (int) newWidth andHeight: (int) newHeight
	andBorder: (Border*) newBorder {
	//call the fourth init method
	return [self initWithX: 0 andY: 0 andWidth: newWidth
			andHeight: newHeight andBorder: newBorder];
}

//fourth init method - initializes a Window with specified coordinates,
//dimensions, and a border
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight andBorder: (Border*) newBorder {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_x = newX;
		_y = newY;
		_width = newWidth;
		_height = newHeight;
		[self create]; //create the window data
		[self setBorder: newBorder]; //set the border
		[self refresh]; //and refresh the window
	}

	//and return the instance
	return self;
}

//copy method - copies a Window instance
- (id) copy {
	//return a copy of the Window instance
	return [[Window alloc] initWithX: _x andY: _y
			andWidth: _width
		       andHeight: _height
		       andBorder: [_border copy]];
}

//dealloc method - deallocates a Window
- (void) dealloc {
	[self removeBorder]; //remove the current border
	[self destroy]; //deallocate the window data
	[_border release]; //release the border field
	[super dealloc]; //and call the superclass dealloc method
}

//maxX method - returns the maximum x-value of the Window
- (int) maxX {
	int x, y; //will hold the maximum values
	getmaxyx(_data, y, x); //populate the max value variables
	return x; //and return the maximum x-value
}

//maxY method - returns the maximum y-value of the Window
- (int) maxY {
	int x, y; //will hold the maximum values
	getmaxyx(_data, y, x); //populate the max value variables
	return y; //and return the maximum y-value
}

//cursorX method - returns the x-coord of the current cursor position
- (int) cursorX {
	int x, y; //will hold the cursor position
	getyx(_data, y, x); //populate the coordinate variables
	return x; //and return the x-coord
}

//cursorY method - returns the y-coord of the current cursor position
- (int) cursorY {
	int x, y; //will hold the cursor position
	getyx(_data, y, x); //populate the coordinate variables
	return y; //and return the y-coord
}

//setBorder method - sets the border for the Window
- (void) setBorder: (Border*) newBorder {
	//release the current border
	[_border release];

	//update the border field
	_border = newBorder;
	if(_border != nil) { 
		[_border retain];
	}

	//update the window border
	if(_border != nil) { //if the new border is not nil
		//then update the window border
		[self refreshBorder];
	} else { //if the new border is nil
		//then recursively create an empty border
		[self setBorder: [[Border alloc] init]];
	}

	//and refresh the window
	[self refresh];
}

//setX method - sets the x-coord of the Window
- (void) setX: (int) newX {
	Border* tmp = [_border copy]; //copy the border
	[self removeBorder]; //remove the current border
	[self destroy]; //destroy the current window data
	_x = newX; //update the x-coord field
	[self create]; //create the window data again
	[self setBorder: tmp]; //restore the border
	[tmp release]; //release the copied border
	[self refresh]; //and refresh the window
}

//setY method - sets the y-coord of the Window
- (void) setY: (int) newY {
	Border* tmp = [_border copy]; //copy the border
	[self removeBorder]; //remove the current border
	[self destroy]; //destroy the current window data
	_y = newY; //update the y-coord field
	[self create]; //create the window data again
	[self setBorder: tmp]; //restore the border
	[tmp release]; //release the copied border
	[self refresh]; //and refresh the window
}

//setWidth method - sets the width of the Window
- (void) setWidth: (int) newWidth {
	Border* tmp = [_border copy]; //copy the border
	[self removeBorder]; //remove the current border
	[self destroy]; //destroy the current window data
	_width = newWidth; //update the width field
	[self create]; //create the window data again
	[self setBorder: tmp]; //restore the border
	[tmp release]; //release the copied border
	[self refresh]; //and refresh the window
}

//setHeight method - sets the height of the Window
- (void) setHeight: (int) newHeight {
	Border* tmp = [_border copy]; //copy the border
	[self removeBorder]; //remove the current border
	[self destroy]; //destroy the current window data
	_height = newHeight; //update the height field
	[self create]; //create the window data again
	[self setBorder: tmp]; //restore the border
	[tmp release]; //release the copied border
	[self refresh]; //and refresh the window
}

//refresh method - refreshes the Window
- (void) refresh {
	wrefresh(_data); //refresh the Window
}

//removeBorder method - removes the Window's border
- (void) removeBorder {
	//get a blank border
	Border* tmp = [[[Border alloc] init] autorelease];
	[self setBorder: tmp]; //set the border field to the blank border
	[self refresh]; //refresh the border
}

//enableKeypad method - enables keypad entry for the Window
- (void) enableKeypad {
	keypad(_data, TRUE); //enable keypad entry
}

//disableKeypad method - disables keypad entry for the Window
- (void) disableKeypad {
	keypad(_data, FALSE); //disable keypad entry
}

//clear method - clears the Window
- (void) clear {
	wclear(_data); //clear the Window
}

//private create method - initializes the data field
- (void) create {
	//initialize the data field
	_data = newwin(_height, _width, _y, _x);
}

//private destroy method - destroys the data field
- (void) destroy {
	delwin(_data); //deallocate the data field
}

//private refreshBorder method - refreshes the window border
- (void) refreshBorder {
	//and update the window border
	wborder(_data, [_border leftSide], [_border rightSide],
		[_border top], [_border bottom],
		[_border topLeft], [_border topRight],
		[_border bottomLeft], [_border bottomRight]);
}

@end 
