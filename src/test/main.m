/*
 * main.m
 * Main code file for the libobjccurses demo program
 * Created on 8/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../lib/objccurses.h"

//global variables
char* choices[] = {
			"Choice 1",
			"Choice 2",
			"Choice 3",
			"Choice 4",
			"Exit"
		  };
int startX = 0;
int startY = 0;
int nChoices = sizeof(choices) / sizeof(char*);

//function prototypes
void printMenu(Window* menuWin, int highlight); //prints a menu
int getChoice(int mouseX, int mouseY); //returns the selected menu choice

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	//create an array with the initialization options
	NSArray* enviroment = [[[NSArray alloc] initWithObjects:
				[InitOption cbreak],
				[InitOption noecho], 
				[InitOption keypad], nil] autorelease];

	//declare a variable to hold the menu choice
	int choice = 0;

	//get a Context object
	Context* ctxt = [[Context alloc] initWithEnviroment: 
				enviroment];

	//make the cursor invisible
	[ctxt makeCursorInvisible];

	//demo message logging
	[ctxt logMessage: @"Demo log message"];

	//clear the screen
	[ctxt clearScreen];	

	//get a mouse event handler
	MouseEventHandler* handler = 
		[[[MouseEventHandler alloc] init] autorelease];

	//get an output stream
	MovableOutputScreenStream* oss =
		[[[MovableOutputScreenStream alloc] init] autorelease];

	//get a reverse attribute
	ScreenAttribute* scrRev = 
		[[ScreenAttribute alloc] initWithType: ATTR_REVERSE];

	//enable it
	[scrRev enable];

	//write an info message to the screen
	[oss writeString: @"Click on Exit to quit\n"];

	//write a diamond to the screen
	int streamX = [oss xPos];
	int streamY = [oss yPos];
	[oss setXPos: 1];
	[oss setYPos: 2];
	[scrRev disable];
	[oss writeAltChar: [AltChar DIAMOND]];
	[scrRev enable];
	[oss setXPos: streamX];
	[oss setYPos: streamY];


	//refresh the screen
	[ctxt refreshScreen];

	//disable the reverse attribute
	[scrRev disable];
	[scrRev release];

	//define the width and height of the window
	int width = 32;
	int height = 12;

	//get the center of the screen
	startY = ([ctxt lineCount] - height) / 2;
	startX = ([ctxt columnCount] - width) / 2;

	//create a Border object to border the window with
	Border* border = [[Border alloc] initWithLeftSide: '|'
				andRightSide: '|'
				      andTop: '-'
				   andBottom: '-'
				  andTopLeft: '+'
				 andTopRight: '+'
			       andBottomLeft: '+'
			      andBottomRight: '+'];

	//create a Window
	Window* win = [[Window alloc] initWithX: startX andY: startY 
					andWidth: width
				       andHeight: height
				       andBorder: border];

	//enable the Window's keypad
	[win enableKeypad];

	//create a movable input window stream
	MovableInputWindowStream* iws =
		[[MovableInputWindowStream alloc] initWithWindow: win
			andXPos: 1 andYPos: 1];

	//print the menu on the window
	printMenu(win, 1);

	//loop and process user input
	while(YES) {
		//get the input character
		int c = [iws readChar];

		//determine whether the character was mouse input
		if(c == [KeyCode mouse]) { //if the mouse was used
			//then process the mouse input
			@try {
				//get the mouse event
				MouseEvent* event = [handler pollEvent];

				//handle the event
				if([event triggeredBy: B1_CLICKED]) {
					choice = getChoice(
							[event x] + 1,
							[event y] + 1);

					//determine whether to
					//exit the loop
					if(choice == -1) {
						goto end;
					}

					//print the choice
					[oss setXPos: 1];
					[oss setYPos: 22];
					NSString* choiceStr =
					[[NSString alloc] initWithFormat:
		@"Choice made is: %d Chosen menu option is \"%s\"",
						choice,
						choices[choice - 1]];
					[oss writeString: choiceStr];
					[ctxt refreshScreen];
					[choiceStr release];
				}
			}
			@catch (MouseEventException* e) {
				[oss setXPos: 0];
				[oss setYPos: 23];
				[oss writeString: [e reason]];
			}

			//print the menu
			printMenu(win, choice);
		}
	}

end:
	//make the cursor visible again
	[ctxt makeCursorVisible];

	//release the input stream
	[iws release];

	//release the window
	[win release];

	//release the border
	[border release];

	//release the context
	[ctxt release];

	//drain the autorelease pool
	[pool drain];

	//and return with no errors
	return EXIT_SUCCESS;
}

//printMenu function - prints a formatted menu
void printMenu(Window* menuWin, int highlight) {
	//get an output stream from the window
	MovableOutputWindowStream* ows =
		[[MovableOutputWindowStream alloc] initWithWindow: menuWin
			andXPos: 1 andYPos: 1];

	int i;
	//loop through the choices
	for(i = 0; i < nChoices; i++) {
		//determine whether to highlight the current choice
		if(highlight == i + 1) {
			//get a window attribute
			WindowAttribute* attr =
				[[WindowAttribute alloc]
				initWithWindow: menuWin
				       andType: ATTR_REVERSE];

			//enable it
			[attr enable];

			//print out the menu option
			[ows writeString: [NSString stringWithFormat:
				@"%s", choices[i]]];

			//disable the attribute
			[attr disable];

			//and release it
			[attr release];
		} else {
			[ows writeString: [NSString stringWithFormat:
				@"%s", choices[i]]];
		}

		[ows setYPos: [ows yPos] + 1];
	}

	//release the stream
	[ows release];
}

//getChoice method - returns a menu choice
int getChoice(int mouseX, int mouseY) {
	int i = startX + 1;
	int j = startY + 2;
	
	int c;
	//loop and get the choice
	for(c = 0; c < nChoices; c++) {
		//determine whether the mouse clicked on the
		//current choice
		if(mouseY == j + c && mouseX >= i &&
			mouseX <= i + strlen(choices[c])) {
			if(c == nChoices - 1) {
				return -1;
			} else {
				return c + 1;
			}
		}
	}

	//appease the compiler
	return -1;
}

//end of program
